const mongoose=require('mongoose');
const MarksSch=new mongoose.Schema({
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'student'
    },
    subject:{
        type:String
    },
    marks:{
        type:String,
    },
},
{
    timestamps:true
})
const MarksDetails=mongoose.model('marks',MarksSch);
module.exports=MarksDetails