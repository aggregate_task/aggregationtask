const mongoose=require('mongoose');
const StudentSch=new mongoose.Schema({
    //reference of college
    collegeId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'college'
    },
    fname:{
        type:String,
    },
    lname:{
        type:String,
    },
    email:{
        type:String,
    },
    phone:{
        type:String
    }
},
{
    timestamps:true
})

const Stu_Details=mongoose.model('student',StudentSch)
module.exports=Stu_Details;