const express=require('express');
require('../config');
const Stu_Details=require('../databases/Student_Sch_Mod');
const CollegeDetails=require('../databases/College_Sch_Mod');
const MarksDetails=require('../databases/Marks_Sch_Mod');
const mongodb=require('mongodb')

// post apis

// post student details
module.exports.postStuDetails= async(req,res)=>{
    try{
        // let data=await Stu_Details.save(req.body);
        let data= await Stu_Details.create({
            fname:req.body.fname,
            lname:req.body.lname,
            email:req.body.email,
            phone:req.body.phone,
            collegeId:req.params
        })
        console.log(data);
        return res.status(200).json({
            message:"created successfully!!",
            data:data})
    }
    catch(error){
        return res.status(400).json({
            message:"error in adding details!!",
            error:error
        })
    }
}

// post college details
module.exports.postCollegeDetails=async(req,res)=>{
    try{
        let data=await CollegeDetails.create({
            college:req.body.college,
            course:req.body.course,
            // userId:req.params
        })
        console.log(data);
        return res.status(200).json({
            message:"created successfully!!",
            data:data
        })
    }
    catch(error){
        return res.status(400).json({
            message:"error in adding details",
            error:error
        })
    }
}

// post marks details
module.exports.postMarksDetails=async(req,res)=>{
    try{
        let data=await MarksDetails.create({
            subject:req.body.subject,
            marks:req.body.marks,
            userId:req.params
        })
        console.log(data);
        return res.status(200).json({
            message:"created successfully!!",
            data:data
        })
    }
    catch(error){
        return res.status(400).json({
            message:"error in adding details",
            error:error
        })
    }
}


// get apis

// get the details of student through marks by id
module.exports.getStuByMarks=async(req,res)=>{
    try
    {
        let data=await MarksDetails.find(req.params)
        // why aggregation:- as it returns a single object while nested population doesn't
        .populate({
            path:'userId',
            populate:{
                path:'collegeId',
                model:'college',
                select:'-createdAt -updatedAt'
            }
        })
        console.log(data);
        return res.status(200).json({
        message:"success!!",
        data:data
        }) 
    }
    catch(error){
        return res.status(400).json({
            message:"error in fetching details!!",
            error:error
        })
    }
}


// get the details of student through college by userId
// module.exports.getStuByCollege=async(req,res)=>{
//     try
//     {
//         let data=await CollegeDetails.find(req.params)
//         .populate("userId","-createdAt -updatedAt")
//         // .populate("userId")
//         // .select('-createdAt');
//         // console.log(data);
//         return res.status(200).json({
//         message:"success!!",
//         data:data
//         }) 
//     }
//     catch(error){
//         return res.status(400).json({
//             message:"error in fetching details!!",
//             error:error
//         })
//     }
// }

module.exports.getCollegeByStu=async(req,res)=>{
    try
    {
        let data=await Stu_Details.find(req.params)
        .populate("collegeId")
        // .populate("userId")
        // .select('-createdAt');
        // console.log(data);
        return res.status(200).json({
        message:"success!!",
        data:data
        }) 
    }
    catch(error){
        return res.status(400).json({
            message:"error in fetching details!!",
            error:error
        })
    }
}

// using aggregate to get all details
module.exports.getaggregate=async(req,res)=>{
    // console.log(req.params);
    let userId=req.params.userId;
    console.log(userId)
    const newid=new mongodb.ObjectId(userId);
    // let newid=userId.toString();
    // console.log(typeof(newid));
    let result=await MarksDetails.aggregate(
        [
            { "$limit" : 1 }, 
            { "$skip" : 0 },
            {$project:{
                createdAt:false, updatedAt:false
            }},
            {$match: {userId:newid}},
        {$lookup:{from:'students',localField:'userId', foreignField:'_id',as:'data'}},
        {$unwind:"$data"},
        {
            $lookup:{from:'colleges',localField:'data.collegeId',foreignField:'_id',as:"results"}
        }
        ,{
            $unwind:
                "$results"
            
        },
        {
            $project: {
                "data.createdAt" : 0
            }
        }
    ])
    return res.json({
        message:'success!!',
        data:result
    })
}