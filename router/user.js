const express=require('express');
const apis=require('./route');
const router=express.Router();

// post apis
router.post('/createStuDetails/:_id',apis.postStuDetails);
router.post('/createCollegeDetails',apis.postCollegeDetails);
router.post('/createMarksDetails/:_id',apis.postMarksDetails);

// getapis
router.get('/getStuByMarks/:userId',apis.getStuByMarks);
// router.get('/getStuByCollege/userId',apis.getCollegeByStu);
router.get('/getCollegeByStu/:collegeId',apis.getCollegeByStu);
router.get('/aggregateall/:userId',apis.getaggregate);
// router.get('/aggregateall',apis.getaggregate);
module.exports=router;