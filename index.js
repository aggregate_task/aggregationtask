const express=require('express');
const router=require('./router/user.js')
const morgan=require('morgan');
const app=express();
app.use(express.json());
app.use(morgan("dev"))
app.use('/',router);
app.listen(6000,()=>{
    require('./config.js')
})